

#pragma once
#include <string>
#include <RsaPublicKeyHolder.h>
#include "AccessCard.h"
#include "log.h"

using namespace std;

class CardParser {
    RsaPublicKeyHolder *publicKeyHolder;
public:
    CardParser(RsaPublicKeyHolder* publicKeyHolder);
    AccessCard parseAndValidateCard(long cardId, string payload);
    bool checkSignature(string data, string signature);
};



