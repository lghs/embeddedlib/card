#pragma once


#include <ArduinoJson.h>
#include <string>
#include "log.h"

using namespace std;

#define ACCESSCARD_CARDID "cardId"
#define ACCESSCARD_USERNAME_FIELD_ID "username"
#define ACCESSCARD_USERID_FIELD_ID "userId"

class AccessCard {
private:
    DynamicJsonDocument jsonDocument;
    bool validCard;
public:
    AccessCard(string jsonContent, bool validCard = false);

    AccessCard(DynamicJsonDocument jsonContent, bool validCard = false);

    string userId();

    string username();

    string desc(){
        return username();
    }

    void dump();

    bool isValid();
};



