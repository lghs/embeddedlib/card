#!/bin/bash

set -ue
set -o pipefail

DIR=$(cd $(dirname $0) && pwd)

INFO_COLOR='\033[0;33m'
ERROR_COLOR='\033[0;3om'
NC='\033[0m' # No Color

info(){
  echo -e ${INFO_COLOR}$@${NC}
}

error(){
  >&2 echo -e ${ERROR_COLOR}$@${NC}
}

PASS="pass"
CARDID=42314
USERNAME="foobar"
USERID="f40eb2c-7c60-4f37-a17b-8589db3d2dde"
PAYLOAD_DATA='{"cardId":'$CARDID', "username":"'$USERNAME'", "userId":"'$USERID'"}'
PAYLOAD_DATA_B64=$(echo -n $PAYLOAD_DATA | base64 | tr -d "\n")

info
echo payload data : $PAYLOAD_DATA
echo payload data base64 : $PAYLOAD_DATA_B64

info generate key pair
openssl genrsa -des3 -passout pass:$PASS -out $DIR/key.pem 2048
openssl rsa -in $DIR/key.pem -passin pass:$PASS -pubout -out $DIR/cert.pem
openssl rsa -in $DIR/key.pem -out $DIR/key.insecure.pem -passin pass:$PASS
openssl pkcs8 -topk8 -inform PEM -in $DIR/key.pem -out $DIR/key.pkcs8.pem -passin pass:$PASS -nocrypt

info sign payload
SIGNATURE=$(openssl dgst -sha256 -sign $DIR/key.insecure.pem <(echo -n $PAYLOAD_DATA_B64) | base64 | tr -d "\n")

info verify signature
openssl dgst -sha256 -verify $DIR/cert.pem -signature <(echo $SIGNATURE | base64 -d ) <(echo -n $PAYLOAD_DATA_B64)

info compute payload
PAYLOAD=$PAYLOAD_DATA_B64.$SIGNATURE

info export info for test
PUBLIC_KEY=$(cat $DIR/cert.pem| tr '\n' '#' | sed 's/#/\\n/g';)
(
cat << EOF
#pragma once

#include<string>
using namespace std;

long cardId=${CARDID};

string username="${USERNAME}";

string userId="${USERID}";

string publicKey="${PUBLIC_KEY}";

string payload="${PAYLOAD}";

EOF
) > $DIR/data.h

info backend config
echo card.publicKey = '"'$(cat $DIR/cert.pem | tr -d "\n")'"'
echo card.privateKey = '"'$(cat $DIR/key.pkcs8.pem | tr -d "\n")'"'
