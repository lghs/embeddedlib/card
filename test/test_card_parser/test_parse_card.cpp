/*
 Copyright (c) 2014-present PlatformIO <contact@platformio.org>
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
**/

#include <unity.h>

#define PRINT_ERROR(s) TEST_MESSAGE((string("ERROR:") + s).c_str())
#define PRINT_DEBUG(s) TEST_MESSAGE((string("DEBUG:") + s).c_str())

#include "data.h"
#include <stdio.h>
#include<string>
#include <iostream>
#include <RsaPublicKeyHolder.h>
#include <CardParser.h>


#define TEST_NO_ERROR(f)            \
try{                                \
    f                               \
} catch (std::runtime_error e){     \
    TEST_FAIL_MESSAGE(e.what());    \
}

RsaPublicKeyHolder key;
CardParser cardParser(&key);

using namespace std;

void setUp(void) {
    key.loadKey(publicKey);
}

void tearDown(void) {
}

void test_read_card(void) {
    TEST_NO_ERROR(
            AccessCard card = cardParser.parseAndValidateCard(cardId, payload);
            TEST_ASSERT_EQUAL_STRING(userId.c_str(), card.userId().c_str());
            TEST_ASSERT_EQUAL_STRING(username.c_str(), card.username().c_str());
            TEST_ASSERT_MESSAGE(card.isValid(), "card should be valid");
    )

}

void test_read_card_invalid_cardId(void) {
    TEST_NO_ERROR(
            AccessCard card = cardParser.parseAndValidateCard(0, payload);
            TEST_ASSERT_MESSAGE(!card.isValid(), "card should be invalid");
    )

}

void test_read_card_invalid_signature(void) {
    TEST_NO_ERROR(
            char copy[payload.size()];
            payload.copy(copy,payload.size());
            copy[payload.size() -3 ] = 'a';
            copy[payload.size() -4 ] = 'a';
            copy[payload.size() -5 ] = 'a';
            AccessCard card = cardParser.parseAndValidateCard(cardId, string(copy));
            TEST_ASSERT_MESSAGE(!card.isValid(), "card should be invalid");
    )

}


void RUN_UNITY_TESTS() {
    UNITY_BEGIN();
    RUN_TEST(test_read_card);
    RUN_TEST(test_read_card_invalid_cardId);
    RUN_TEST(test_read_card_invalid_signature);
    UNITY_END();
}



int main(int argc, char **argv) {
    string programPath = argv[0];

    RUN_UNITY_TESTS();
    return 0;
}


