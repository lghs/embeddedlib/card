

#include <Base64.h>
#include "CardParser.h"

template< class T, class = typename std::enable_if<
        std::is_integral<T>::value ||
        std::is_floating_point<T>::value>::type>
typename std::string to_string(T t)
{

    return string("to_string broken");

}

CardParser::CardParser(RsaPublicKeyHolder *publicKeyHolder) : publicKeyHolder(publicKeyHolder) {

}

AccessCard CardParser::parseAndValidateCard(long cardId, string payload) {

    size_t pos = payload.find(".");
    string data = payload.substr(0, pos);
    string signature = payload.substr(pos + 1);


    PRINT_DEBUG("check signature on " + data + " with signature " + signature)
    bool validSignature = checkSignature(data, signature);
    PRINT_DEBUG("check signature : " + to_string(validSignature))

    PRINT_DEBUG("deserialize payload : ")
    string decodedPayload = Base64::decodeString(data);
    DynamicJsonDocument jsonContent(2046);
    DeserializationError ret = deserializeJson(jsonContent, decodedPayload);
    if (ret != DeserializationError::Ok) {
        throw std::runtime_error(string("Error while reading payload : ") + string(ret.c_str()));
    }
    PRINT_DEBUG("payload deserialized : " + to_string(validSignature))

    bool validId = false;
    if (jsonContent.containsKey(ACCESSCARD_CARDID)) {
        long payloadCardId = jsonContent[ACCESSCARD_CARDID];
        validId = payloadCardId == cardId;
        if (!validId) {
            const string basicString =
                    string("cardId in payload is not correct, inPayload : ") +
                    to_string(payloadCardId) + string(" card:") + to_string(cardId);
            PRINT_ERROR(basicString);
        }
    } else {
        PRINT_ERROR(string("no cardid in payload"));
    }

    AccessCard card = AccessCard(jsonContent, validSignature && validId);

    return card;
}

bool CardParser::checkSignature(string data, string signature) {
    try {
        Sha256RsaSignature sig = Sha256RsaSignature::ofBase64(signature);
        publicKeyHolder->verifySignature(data, sig);
        return true;
    } catch (std::runtime_error e) {
        PRINT_ERROR(string("errror while validating card : " + string(e.what())));
        return false;
    }
}