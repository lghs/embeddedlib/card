

#include "AccessCard.h"

AccessCard::AccessCard(string jsonContent, bool validCard) :
        jsonDocument(2048), validCard(validCard) {
    deserializeJson(jsonDocument, jsonContent);
}

AccessCard::AccessCard(DynamicJsonDocument jsonContent, bool validCard) :
        jsonDocument(jsonContent), validCard(validCard) {
}



string AccessCard::userId() {
    if(jsonDocument.containsKey(ACCESSCARD_USERID_FIELD_ID)){
        return jsonDocument[ACCESSCARD_USERID_FIELD_ID];
    } else {
        return "";
    }
}

string AccessCard::username() {
    if(jsonDocument.containsKey(ACCESSCARD_USERNAME_FIELD_ID)){
        return jsonDocument[ACCESSCARD_USERNAME_FIELD_ID];
    } else {
        return string("");
    }
}

bool AccessCard::isValid() {
    return validCard;
}
template< class T>
typename std::string to_string(T t)
{
    return string("to_string broken");
}

void AccessCard::dump() {
    string ret=string("Card content ");

    string out;
    serializeJson(jsonDocument, out);
    ret.append(out.c_str());

    ret.append( " userId:");
    ret.append(userId());

    ret.append(" username:");
    ret.append(username().c_str());

    ret.append(" isValid:");
    ret.append(to_string(validCard));

    PRINT_DEBUG(ret);
}


